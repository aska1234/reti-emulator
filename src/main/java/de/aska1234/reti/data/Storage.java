package de.aska1234.reti.data;

import java.util.Set;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class representing a Storage for a ReTI program to work on
 */
public class Storage {

    /**
     * Enum representing the 4 registers of ReTI
     */
    public enum Registers {
        ACC,IN1,IN2,PC
    }

    // The storage data. Is a map because arbitrary indexes can be set without having
    // nothing all over the place just to fill the gaps
    private Map<Integer,Integer> data;

    // The registers of the ReTI
    private Map<Registers,Integer> registers;

    /**
     * Default constructor.
     * Initializes the storage and registers without holding data except PC which is initialized with 0.
     */
    public Storage() {
        this.data = new HashMap<>();
        this.registers = new HashMap<>();
        this.registers.put(Registers.PC, 0);
    }

    /**
     * Initializes the storage empty and registers as given.
     * Behaves like {@code setInitReg}
     * @param initReg the data to initialize the registers with. Must not be {@code null}
     */
    public Storage(Map<Registers, Integer> initReg) {
        this.data = new HashMap<>();
        this.setInitReg(initReg);
    }

    /**
     * Initializes the storage and registers as given.
     * Behaves like {@code setInitReg} and {@code setInitData}
     * @param initReg the data to initialize the registers with. Defaults to not set and PC set to 0 if {@code null}.
     * @param initData the data to initialize the storage with. Must not be {@code null}.
     */
    public Storage(Map<Registers,Integer> initReg, Map<Integer, Integer> initData) {
        if (initReg != null)
            this.setInitReg(initReg);
        else {
            this.registers = new HashMap<>();
            this.registers.put(Registers.PC, 0);
        }
        this.setInitData(initData);
    }

    /**
     * Set storage cell with index {@code index} to value {@code data}.
     * @param index The index to set
     * @param data The data to store
     */
    public void set(int index, int data) {
        this.data.put(index,data);
    }

    /**
     * Set register {@code register} to value {@code data}.
     * @param register the register to set
     * @param data the data to store
     */
    public void set(Registers register, int data) {
        this.registers.put(register,data);
    }

    /**
     * Get the data stored at {@code index}.
     * Throws {@code IllegalArgumentException} if storage cell at {@code index} is not set.
     * @param index the index to read
     * @return the value stored at {@code index}
     */
    public int get(int index) {
        if (!this.data.containsKey(index))
            throw new IllegalArgumentException("Storage cell " + index + " not initialized");
        return this.data.get(index);
    }

    /**
     * Get the data stored in register {@code register}.
     * Throws {@code IllegalArgumentException} if {@code register} is not set.
     * @param register the register to read.
     * @return the value stored in register {@code register}
     */
    public int get(Registers register) {
        if (!this.registers.containsKey(register))
            throw new IllegalArgumentException("Register " + register.name() + " not initialized");
        return this.registers.get(register);
    }

    /**
     * Empty the storage and registers and reset PC to 0.
     */
    public void reset() {
        this.data = new HashMap<>();
        this.registers = new HashMap<>();
        this.registers.put(Registers.PC, 0);
    }

    /**
     * Reinitialize the storage with {@code initData}.
     * Copies the content of the given data. Changing the given map afterward will <b>not</b>
     * change the content of the storage
     * @param initData the data to set. Must not be {@code null}.
     */
    public void setInitData(Map<Integer,Integer> initData) {
        if (initData == null)
            throw new IllegalArgumentException("initial storage must not be null");
        this.data = new HashMap<>();
        this.data.putAll(initData);
    }

    /**
     * Reinitialize the registers with {@code initReg}.
     * Copies the content of the given data. Changing the given map afterward will <b>not</b>
     * change the content of the registers.
     * @param initReg the data to set. must not be {@code null}.
     */
    public void setInitReg(Map<Registers,Integer> initReg) {
        if (initReg == null)
            throw new IllegalArgumentException("initial register must not be null");
        this.registers = new HashMap<>();
        this.registers.putAll(initReg);
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        Set<Integer> keys = data.keySet();
        List<Integer> sortedKeys = new ArrayList<>(keys);
        sortedKeys.sort(Integer::compareTo);
        for (int key: sortedKeys) {
            out.append(String.format("%11d:%11d\n", key, data.get(key)));
        }
        return out.toString();
    }
}
