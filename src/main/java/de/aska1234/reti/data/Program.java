package de.aska1234.reti.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
import de.aska1234.reti.data.Storage.Registers;

/**
 * Class representing a ReTI program
 */
public class Program {

    private final List<String> program = new ArrayList<>();

    /**
     * default constructor. Initializes with empty program
     */
    public Program() {}

    /**
     * Initialize the program with the given program if valid.
     * Throws IllegalArgumentException if program is invalid.
     * @param program the program lines
     */
    public Program(List<String> program) {
        for (String line : program) {
            if (!verify(line)) throw new IllegalArgumentException("invalid program");
        }
        this.program.addAll(program);
    }

    /**
     * Initialize the program with the given program if valid.
     * Throws IllegalArgumentException if program is invalid.
     * @param program the program
     */
    public Program(String program) {
        if (!verify(program)) throw new IllegalArgumentException("invalid program");
        List<String> lines = List.of(program.split("\\n"));
        this.program.addAll(lines);
    }

    /**
     * Add a line of code at the end of the program.
     * Does nothing if the line is invalid
     * @param line the line to add
     * @return whether the provided line of code is valid
     */
    public boolean add(String line) {
        if (!verifyLine(line)) return false;
        this.program.add(line);
        return true;
    }

    /**
     * Get a specific line of code.
     * @param line the line number of the desired line
     * @return the requested line of code if it exists
     */
    public String get(int line) {
        return this.program.get(line);
    }

    /**
     * insert a line of code so that is hast the specified line number afterward.
     * Does nothing if the line is invalid.
     * @param line the line number to add at
     * @param code the line of code to add
     * @return whether the provided line of code is valid
     */
    public boolean insert(int line, String code) {
        if (!verifyLine(code)) return false;
        int size = this.program.size();
        // add the last line again to the end
        this.program.add(this.program.get(this.program.size() - 1));
        // move every line from the index on one line down
        for (int i = size; i > line; i--) {
            this.program.set(i, this.program.get(i-1));
        }
        this.program.set(line, code);
        return true;
    }

    /**
     * Check whether a given line of code is valid.
     * Lines with only whitespace and line that only have a comment
     * are considered valid.
     * @param line the line of code to check
     * @return whether the line of code is valid
     */
    public static boolean verifyLine(String line) {
        // split line into arguments by whitespace, removing empty result to allow for multiple whitespaces without breaking stuff
        List<String> temp_parts = List.of(line.split("\\s"));
        List<String> parts = temp_parts.stream().filter((x) -> !x.isEmpty()).map(Object::toString).toList();

        // if line is empty or comment only it is valid
        if (parts.size() == 0 || parts.get(0).startsWith("%")) return true;

        return switch (parts.get(0).toUpperCase()) {
            // these commands take an optional destination register and a whole number as arguments
            case "LOAD", "LOADIN1", "LOADIN2", "LOADI", "SUBI", "ADDI", "OPLUSI", "ORI", "ANDI", "SUB", "ADD", "OPLUS", "OR", "AND" ->
                    // Option 1: Destination register given (at least 2 args after command, counting comments)
                    (parts.size() >= 3
                            // First argument is Register name
                            && Arrays.stream(Registers.values()).map(Enum::name).anyMatch(parts.get(1)::equals)
                            // Second argument is whole number
                            && parts.get(2).matches("-?[0-9]+")
                            // Third argument starts a comment if it exists
                            && (parts.size() == 3 || parts.get(3).startsWith("%")))
                    // Option 2: Destination register omitted (at least 1 arg after command, counting comments)
                    || (parts.size() >= 2
                            // First argument is whole number
                            && parts.get(1).matches("-?[0-9]+")
                            // Second argument starts a comment if it exists
                            && (parts.size() == 2 || parts.get(2).startsWith("%")));
            // these commands take a whole number as argument
            case "STORE", "STOREIN1", "STOREIN2", "JUMPGT", "JUMPEQ", "JUMPGEQ", "JUMPLT", "JUMPNEQ", "JUMPLEQ", "JUMP" ->
                    // At least 1 arg after command, counting comments
                    parts.size() >= 2
                            // First argument is whole number
                            && parts.get(1).matches("-?[0-9]+")
                            // Second argument starts a comment if it exists
                            && (parts.size() == 2 || parts.get(2).startsWith("%"));
            // these commands take a source register and a destination register as arguments
            case "MOVE" ->
                    // At least 2 args after command, counting comments
                    parts.size() >= 3
                            // First argument is Register name
                            && Arrays.stream(Registers.values()).map(Enum::name).anyMatch(parts.get(1)::equals)
                            // Second argument is Register name
                            && Arrays.stream(Registers.values()).map(Enum::name).anyMatch(parts.get(2)::equals)
                            // Third argument starts a comment if it exists
                            && (parts.size() == 3 || parts.get(3).startsWith("%"));
            // these commands take no arguments
            case "NOP" ->
                    // First argument starts a comment if it exists
                    parts.size() == 1 || parts.get(1).startsWith("%");
            // if there is some non-comment stuff that didn't match it's not a comment or a command and therefore invalid
            default -> false;
        };
    }

    /**
     * Check whether a given program code is valid.
     * Lines that only contain whitespaces or that only have a comment
     * are considered valid.
     * @param code the program code to check
     * @return whether the program is valid
     */
    public static boolean verify(String code) {
        String[] lines = code.split("\\n");
        for (String line : lines) {
            if (!verifyLine(line)) return false;
        }
        return true;
    }

    /**
     * Execute one line of code on the given storage.
     * Which line of code is to be executed is determined by the program counter register
     * in the given storage.
     * @param storage the storage to execute the line of code on
     * @return whether the executed line has caused a change on the storage including the registers.
     */
    public boolean executeOnce(Storage storage) {
        // get the line of code and pre-calculate the new PC
        int pc = storage.get(Registers.PC);
        int newPc = pc + 1;
        String command = program.get(pc);

        // split the line of codes into its pieces
        List<String> parts = Stream.of(command.split("\\s")).filter((x) -> !x.isEmpty()).map(Object::toString).toList();

        // if the line is empty or only a comment, skip execution and just increase the PC,
        // returning true as the PC has changed
        if (parts.size() == 0 || parts.get(0).startsWith("%")) {
            storage.set(Registers.PC, newPc);
            return true;
        }

        // determine the destination register (default to ACC if none is specified but may not be used in those cases)
        Registers dest = switch (parts.get(0).toUpperCase()) {
            // for these commands the destination register is the first argument if it has 2 arguments or implied to be ACC if it has only one argument
            case "LOAD", "LOADIN1", "LOADIN2", "LOADI", "SUBI", "ADDI", "OPLUSI", "ORI", "ANDI", "SUB", "ADD", "OPLUS", "OR", "AND" ->
                    parts.size() >= 3 && !parts.get(2).startsWith("%") ? Registers.valueOf(parts.get(1).toUpperCase()) : Registers.ACC;
            // for these commands the destination register is the second argument
            case "MOVE" -> Registers.valueOf(parts.get(2).toUpperCase());
            default -> Registers.ACC;
        };

        // get the immediate value provided with the command (defaults to 0 if the command has none but is not used in those cases)
        int imm = switch (parts.get(0).toUpperCase()) {
            // for these commands the immediate value is the second argument if it has 2 arguments or the first if it only has 1
            case "LOAD", "LOADIN1", "LOADIN2", "LOADI", "SUBI", "ADDI", "OPLUSI", "ORI", "ANDI", "SUB", "ADD", "OPLUS", "OR", "AND" ->
                    parts.size() >= 3 && !parts.get(2).startsWith("%") ? Integer.parseInt(parts.get(2)) : Integer.parseInt(parts.get(1));
            // for these commands the immediate value is the first argument
            case "STORE", "STOREIN1", "STOREIN2", "JUMPGT", "JUMPEQ", "JUMPGEQ", "JUMPLT", "JUMPNEQ", "JUMPLEQ", "JUMP" ->
                    Integer.parseInt(parts.get(1));
            default -> 0;
        };

        // get the index register used for the command (defaults to IN1, may not be used)
        Registers offset = switch (parts.get(0)) {
            // these commands use the second index register
            case "LOADIN2", "STOREIN2" -> Registers.IN2;
            // everything else uses the first index register or none at all
            default -> Registers.IN1;
        };

        // get the value of the destination register unless the command is only writing to it if at all
        int val1 = !(parts.get(0).startsWith("LOAD") || parts.get(0).startsWith("MOVE") || parts.get(0).equals("JUMP")) ? storage.get(dest) : 0;
        // get the second value to compute with
        int val2 = switch (parts.get(0)) {
            // the second value for the none immediate compute commands and the "normal" LOAD command is the
            // value of the storage cell at the given index
            case "LOAD", "SUB", "ADD", "OPLUS", "OR", "AND" -> storage.get(imm);
            // the second value for the LOADINx commands is the value of the storage cell
            // at the given index offset by the value of the index register
            case "LOADIN1", "LOADIN2" -> storage.get(imm + storage.get(offset));
            // the second value of STOREINx commands is the given index offset by the value
            // of the index register
            case "STOREIN1", "STOREIN2" -> imm + storage.get(offset);
            // the second value of the MOVE command is the value of the given source register
            case "MOVE" -> storage.get(Registers.valueOf(parts.get(1).toUpperCase()));
            // all other commands (JUMP, LOADI, STORE and immediate compute commands) directly use the given value as second value
            default -> imm;
        };

        // compute the JUMP target if the jump condition is met
        if (parts.get(0).startsWith("JUMP")) {
            if (parts.get(0).endsWith("NEQ")) {
                if (val1 != 0) {
                    newPc = pc + val2;
                }
            } else if (parts.get(0).endsWith("GEQ")) {
                if (val1 >= 0) {
                    newPc = pc + val2;
                }
            } else if (parts.get(0).endsWith("LEQ")) {
                if (val1 <= 0) {
                    newPc = pc + val2;
                }
            } else if (parts.get(0).endsWith("EQ")) {
                if (val1 == 0) {
                    newPc = pc + val2;
                }
            } else if (parts.get(0).endsWith("LT")) {
                if (val1 < 0) {
                    newPc = pc + val2;
                }
            } else if (parts.get(0).endsWith("GT")) {
                if (val1 > 0) {
                    newPc = pc + val2;
                }
            } else if (parts.get(0).endsWith("JUMP")) {
                newPc = pc + val2;
            }
        }

        // if we do not wish to overwrite the PC register we can write the new
        // PC now as we do have calculated the JUMP if any
        if (dest != Registers.PC) {
            storage.set(Registers.PC, newPc);
        }

        // actually compute the command and store the result
        switch (parts.get(0).toUpperCase()) {
            // put the loaded value into the destination
            case "MOVE", "LOAD", "LOADI", "LOADIN1", "LOADIN2" -> storage.set(dest, val2);
            // STORE the loaded value to the calculated storage cell
            case "STORE", "STOREIN1", "STOREIN2" -> storage.set(val2, val1);
            // compute the compute commands and store the result
            case "SUBI", "SUB" -> storage.set(dest, val1 - val2);
            case "ADDI", "ADD" -> storage.set(dest, val1 + val2);
            case "OPLUSI", "OPLUS" -> storage.set(dest, val1 ^ val2);
            case "ORI", "OR" -> storage.set(dest, val1 | val2);
            case "ANDI", "AND" -> storage.set(dest, val1 & val2);
        }

        // as everything that does change something other than the PC
        // increments the PC by one there was no change whatsoever if the PC did not change
        return newPc != pc;
    }

    /**
     * Execute the program on the given storage until a line of code is reached that does not
     * change the storage, including the registers.
     * @param storage the storage to execute the program on
     */
    public void execute(Storage storage) {
        // repeatedly execute the current line until nothing changes anymore
        while (executeOnce(storage));
    }
}
