package de.aska1234.reti;

import de.aska1234.reti.data.Program;
import de.aska1234.reti.data.Storage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        StringBuilder input = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            String line;
            while ((line = br.readLine()) != null) {
                input.append(line).append("\n");
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        String code = input.toString();

        Map<Integer,Integer> initData = new HashMap<>();
        for (String arg : args) {
            if (!arg.matches("-?[0-9]+:-?[0-9]+"))
                throw new IllegalArgumentException("Wrong cli arg format: " + arg + ". Use <index>:<value> for every argument.");
            String[] argParts = arg.split(":");
            int first = Integer.parseInt(argParts[0]);
            int second = Integer.parseInt(argParts[1]);
            initData.put(first,second);
        }

        Storage storage = new Storage(null, initData);

        Program program = new Program(code);
        program.execute(storage);
        System.out.println(storage);
    }
}