#!/bin/sh
cd build/libs || exit
for f in *.jar ; do
  mv "$f" "../../${f%-all.jar}.jar"
done